package sample;

import com.fazecast.jSerialComm.*;
import cz.kamenitxan.sceneswitcher.SceneSwitcher;
import javafx.application.Platform;
import sample.gui.scenes.Home;
import sample.gui.scenes.Login;
import sample.gui.scenes.Rekeningen.Betaal.Opnemen;
import sample.gui.scenes.Rekeningen.Betaal.Overmakenbetaal;
import sample.gui.scenes.Rekeningen.Spaar.OvermakenSpaar;
import sample.gui.scenes.Rfid;

import java.sql.SQLException;

public class Serial {
    DatabaseBS databaseBS = new DatabaseBS();
    Rfid rfid = new Rfid();
    Login login = new Login();
    Home home = new Home();
    Opnemen opnemen = new Opnemen();
    Overmakenbetaal overmakenbetaal = new Overmakenbetaal();
    OvermakenSpaar overmakenspaar   = new OvermakenSpaar();
    public static String Result = "";
    private static String currentScene = "Rfid";
    private String Code = "";
    public String User="";
    String UID ="";
    public void lmao() {

        System.out.println("Kut programma");
        SerialPort comPort = SerialPort.getCommPorts()[0];
        comPort.setBaudRate(2000000);
        comPort.openPort();
        System.out.println("Connected to port");
        comPort.addDataListener(new SerialPortDataListener() {

            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }

            @Override
            public void serialEvent(SerialPortEvent event) {

                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return;
                byte[] newData = new byte[comPort.bytesAvailable()];
                comPort.readBytes(newData, newData.length);

                for (byte temp : newData) {
                    Result += (char) temp;
                }
                System.out.println(Result);


                    checkInput(Result);

                Result = "";
            }
        });

    }

    void checkInput(String Result) {
        System.out.println("Current scene = " + currentScene);
        //System.out.println("Checkinput is called");
        System.out.println("Result = " + Result);
        switch (currentScene) {
            case "account":

                break;
            case "Rfid":
                System.out.println("Voor Query");
                try {
                    UID =databaseBS.executeQuery("Select id From cards where id =" +Result +";", "id").get(0);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("Na query");
                System.out.println(UID);






                break;
            case "Home":

                switch (Result) {
                    case "A":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("rBetaal", "cs"));
                        break;

                    case "B":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("rSpaar", "cs"));
                        break;

                    case "#":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Rfid", "cs"));


                }

                break;
            case "Login":

                if (Result.contains("#")) {
                    Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Rfid", "cs"));
                }

                if (Result.contains("*")) {
                    Code = "";
                } else {
                    if (!Result.contains("D")) {
                        Code += Result;
                        System.out.println("Code is " + Code);
                    } else {


                    }
                }
                break;

            case "rBetaal":
                switch (Result) {
                    case "A":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Overmakenbetaal", "cs"));
                        break;

                    case "B":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Opnemen", "cs"));
                        break;

                    case "#":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Rfid", "cs"));

                        break;
                    case "*":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Home", "cs"));
                        break;


                }
                break;

            case "rSpaar":
                switch (Result) {
                    case "A":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("OvermakenSpaar", "cs"));
                        break;

                    case "#":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Rfid", "cs"));

                        break;
                    case "*":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Home", "cs"));
                        break;


                }
                break;
            case "Opnemen":
                switch (Result) {
                    case "A":
                        opnemen.Honderd();
                        System.out.println("Honderd opgenomen");
                        overmakenbetaal.setText();
                        break;

                    case "B":
                        opnemen.Vijftig();
                        System.out.println("vijftig opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Opnemen", "cs"));
                        break;

                    case "C":
                        opnemen.Twintig();
                        System.out.println("twintig opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Opnemen", "cs"));
                        break;
                    case "D":
                        opnemen.maxSaldo();
                        System.out.println("max opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Opnemen", "cs"));
                    case "*":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("rBetaal", "cs"));
                        break;
                    case "#":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Rfid", "cs"));
                }
                break;

            case "OvermakenBetaal":

                switch (Result) {
                    case "A":
                        overmakenbetaal.Honderd();
                        System.out.println("Honderd opgenomen");

                        overmakenbetaal.setText();
                        break;

                    case "B":

                        overmakenbetaal.Vijftig();
                        System.out.println("vijftig opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Overmakenbetaal","cs"));
                        break;

                    case "C":
                        overmakenbetaal.Twintig();
                        System.out.println("twintig opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Overmakenbetaal", "cs"));
                        break;
                    case "D":
                        overmakenbetaal.maxSaldo();
                        System.out.println("max opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Overmakenbetaal", "cs"));
                    case "*":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("rBetaal", "cs"));
                        break;
                    case "#":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Rfid", "cs"));
                }
                break;

            case "OvermakenSpaar":
                switch (Result) {
                    case "A":
                        overmakenspaar.Honderd();
                        System.out.println("Honderd opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("OvermakenSpaar", "cs"));
                        break;

                    case "B":
                        overmakenspaar.Vijftig();
                        System.out.println("vijftig opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("OvermakenSpaar", "cs"));
                        break;

                    case "C":
                        overmakenspaar.Twintig();
                        System.out.println("twintig opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("OvermakenSpaar", "cs"));
                        break;
                    case "D":
                        overmakenspaar.maxSaldo();
                        System.out.println("max opgenomen");
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("OvermakenSpaar", "cs"));
                    case "*":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("rSpaar", "cs"));
                        break;
                    case "#":
                        Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Rfid", "cs"));
                }

        }

    }

    public void setCurrentScene(String current) {
        currentScene = current;
    }

    public String getCurrentScene() {
        return currentScene;
    }

    public void setCode(String code_)
    {Code = code_;}

}

