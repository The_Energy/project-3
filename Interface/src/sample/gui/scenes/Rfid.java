package sample.gui.scenes;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import cz.kamenitxan.sceneswitcher.SceneSwitcher;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.paint.Color;
import javafx.scene.control.Label;
import sample.Serial;


public class Rfid extends Thread {
    int Attempts = 3;
    String pw = ("1111");
    static String result = "";
    public static Boolean Card = false;
    @FXML
    public static Label rfidLabel;
    @FXML
    private Label lblLabel;

    @FXML
    private Label Timer;


    public void initialize() {

            Serial serial = new Serial();
            serial.setCurrentScene("Rfid");
            serial.setCode("");
            System.out.println("Current Scene " +serial.getCurrentScene());

    }


    public void rfidChecker(String Result) {
        if (Result.contains("Code")) {
            keypadChecker(Result);
        } else {
            if (Result.contains("RFID")) {
                System.out.println(Result);
                System.out.println("RFID CHECKED");
                if (Result.contains("RFID  A5 29 08 85")) {

                    Platform.runLater(() -> SceneSwitcher.getInstance().loadScene("Login", "cs"));

                } else {
                    rfidLabel.setText("Unknown Card!");
                    rfidLabel.setTextFill(Color.rgb(210, 39, 30));


                }
            }
        }
    }
    public void keypadChecker(String code) {
        System.out.println("Functie aangeroepen");
        System.out.println(code);
        if (code == pw) {
            SceneSwitcher.getInstance().loadScene("Home");

        } else {
            System.out.println("code isnt pw");
            lblLabel.setText("Login failed!");
            lblLabel.setTextFill(Color.rgb(210, 39, 30));
            Attempts--;


            if (Attempts != 0) {
                Timer.setText("U heeft nog " + Attempts + " pogingen.");
            } else {

                lblLabel.setText("Je hebt te vaak geprobeerd");
                Timer.setText("Wacht 10 seconden voor uw volgende poging");



            }
        }

    }

}
