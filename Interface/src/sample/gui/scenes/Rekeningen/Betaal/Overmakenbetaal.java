package sample.gui.scenes.Rekeningen.Betaal;;
import cz.kamenitxan.sceneswitcher.SceneSwitcher;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import sample.Serial;
import sample.gui.scenes.Home;

//OVERMAKEN VAN BETAAL NAAR SPAARREKENING

public class Overmakenbetaal {



    @FXML
    private Label rBetaalSaldo;
    @FXML
    private Label rSpaarSaldo;
    @FXML
    private Button maxSaldo;
    public static void main(String[] args) {

    }

    public void initialize() {

        Home Home = new Home();
        Serial serial = new Serial();
        serial.setCurrentScene("OvermakenBetaal");
        System.out.println("Overmaken aangeroepen");
        rBetaalSaldo.setText(String.valueOf(Home.getBalB()));
        System.out.println("Set Betaalsaldo");
        rSpaarSaldo.setText(String.valueOf(Home.getBalS()));
        System.out.println("Set SpaarSaldo");
        maxSaldo.setText(String.valueOf(Home.getBalB() + " (D)"));
        System.out.println("Overmaken aangeroepen na de setText");
        if (rBetaalSaldo == null){
            System.out.println("null");
        }
        if (rSpaarSaldo == null){
            System.out.println("Spaar Null");
        }
        if (maxSaldo == null){
            System.out.println("null");
        }

    }

    public void setText(){
        System.out.println("Overmaken aangeroepen");
        rBetaalSaldo.setText(String.valueOf(Home.getBalB()));
        System.out.println("Set Betaalsaldo");
        rSpaarSaldo.setText(String.valueOf(Home.getBalS()));
        System.out.println("Set SpaarSaldo");
        maxSaldo.setText(String.valueOf(Home.getBalB() + " (D)"));
        System.out.println("Overmaken aangeroepen na de setText");
    }

    public void logout() {
        System.out.println("Naar login");
        SceneSwitcher.getInstance().loadScene("Login", "cs");
    }
    public void Honderd(){

        if(Home.getBalB()-100<0){
            System.out.println("Onvoldoende saldo");
        }
        else {
            Home.setBalS(Home.getBalS() + 100);
            Home.setBalB(Home.getBalB() - 100);


        }


    }
    public void Vijftig(){

        if(Home.getBalB()-50<0){
            System.out.println("Onvoldoende saldo");
        }
        else {
            Home.setBalS(Home.getBalS() + 50);
            Home.setBalB(Home.getBalB() - 50);

            System.out.println("Bedrag over gemaakt");
        }
    }

    public void Twintig(){

        if(Home.getBalB()-20<0){
            System.out.println("Onvoldoende saldo");
        }
        else {
            Home.setBalS(Home.getBalS() + 20);
            Home.setBalB(Home.getBalB() - 20);


            System.out.println("Bedrag over gemaakt");
        }
    }

    public void maxSaldo(){

        if(Home.getBalB()-Home.getBalB()<0){
            System.out.println("Onvoldoende saldo");
        }
        else {
            Home.setBalS(Home.getBalS() + Home.getBalB());
            Home.setBalB(Home.getBalB() - Home.getBalB());

            System.out.println("Bedrag over gemaakt");
        }
    }

    public void Terug(){
        SceneSwitcher.getInstance().loadScene("rBetaal", "cs");
    }

    }
