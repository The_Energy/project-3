package sample.gui.scenes.Rekeningen.Betaal;;
import cz.kamenitxan.sceneswitcher.SceneSwitcher;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import sample.Serial;
import sample.gui.scenes.Home;

//OPNEMEN VAN BETAAL NAAR SPAARREKENING

public class Opnemen {




    @FXML
    private Label rBetaalSaldo;


    @FXML
    private Button maxSaldo;
    public static void main(String[] args) {

    }

    public void initialize() {
            Home Home = new Home();
            Serial serial = new Serial();
            serial.setCurrentScene("Opnemen");


        rBetaalSaldo.setText(String.valueOf(Home.getBalB()));

        maxSaldo.setText(String.valueOf(Home.getBalB() + " (D)"));
    }


    public void logout() {
        System.out.println("Naar login");
        SceneSwitcher.getInstance().loadScene("Login", "cs");
    }
    public void Honderd(){

        if(Home.getBalB()-100<0){
            System.out.println("Onvoldoende saldo");
        }
        else {

            Home.setBalB(Home.getBalB() - 100);


            System.out.println("Een Bedrag van " +100+ " is gepint");
        }


    }
    public void Vijftig(){

        if(Home.getBalB()-50<0){
            System.out.println("Onvoldoende saldo");
        }
        else {

            Home.setBalB(Home.getBalB() - 50);


            System.out.println("Een Bedrag van " +50+ " is gepint");
        }
    }

    public void Twintig(){

        if(Home.getBalB()-20<0){
            System.out.println("Onvoldoende saldo");
        }
        else {

            Home.setBalB(Home.getBalB() - 20);


            System.out.println("Een Bedrag van " +20+ " is gepint");
        }
    }

    public void maxSaldo(){

        if(Home.getBalB()-Home.getBalB()<0){
            System.out.println("Onvoldoende saldo");
        }
        else {

            Home.setBalB(Home.getBalB() - Home.getBalB());

            System.out.println("Een Bedrag van " +Home.getBalB()+ " is gepint");
        }
    }

    public void Terug(){
        SceneSwitcher.getInstance().loadScene("rBetaal", "cs");
    }

}
