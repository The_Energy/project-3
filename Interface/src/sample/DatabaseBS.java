package sample;

import java.sql.*;
import java.util.ArrayList;

import static com.sun.activation.registries.LogSupport.log;

public class DatabaseBS {
    public Connection connection;

    public void connect() {
        try {

            //log("Connecting to database...");
            //connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bankdb", "root", "");
            connection = DriverManager.getConnection("jdbc:mysql://145.24.222.39:8008/bank", "bank", "dPZgvJT5yq4P4ERc");
            //log("Connected to database.");
            System.out.println("Connection made");
        } catch (Exception e) {
            System.out.println("error connection");
            System.out.println("Error: " + e.getMessage());
        }
    }
    public boolean executeQuery(String query) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {

            return false;
        }
        return true;
    }


    public ArrayList<String> executeQuery(String query, String... columns) throws SQLException {
        ArrayList<String> result = new ArrayList<>();

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        resultSet.next();
        for (String column : columns) {
            result.add(resultSet.getString(column));
        }

        return result;
    }


    public ArrayList<String> executeQueryMultipleRows(String query, String column) throws SQLException {
        ArrayList<String> result = new ArrayList<>();

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            result.add(resultSet.getString(column));
        }

        return result;
    }


}
