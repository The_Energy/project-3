package sample;

import cz.kamenitxan.sceneswitcher.SceneSwitcher;
import javafx.application.Application;
import sample.Serial;
import javafx.stage.Stage;


public class Main extends Application {
    private final SceneSwitcher sceneSwitcher = SceneSwitcher.getInstance();

    @Override


    public void start(Stage stage) throws Exception {
        Serial serial = new Serial();
        //serial.lmao();
        DatabaseBS database = new DatabaseBS();
        database.connect();
        //Start
        sceneSwitcher.addScene("Home", "home.fxml");
        sceneSwitcher.addScene("account", "account.fxml");
        sceneSwitcher.addScene("Login", "Login.fxml");
        //Betaal
        sceneSwitcher.addScene("rBetaal", "Rekeningen/Betaal/rBetaal.fxml");
        sceneSwitcher.addScene("Opnemen", "Rekeningen/Betaal/Opnemen.fxml");
        sceneSwitcher.addScene("Overmakenbetaal", "Rekeningen/Betaal/Overmakenbetaal.fxml");
        //Spaar
        sceneSwitcher.addScene("rSpaar","Rekeningen/Spaar/rSpaar.fxml");
        sceneSwitcher.addScene("OvermakenSpaar", "Rekeningen/Spaar/OvermakenSpaar.fxml");
        sceneSwitcher.addScene("Rfid", "Rfid.fxml");
        stage.setTitle("Home screen");
        stage.setScene(sceneSwitcher.createMainScene(this.getClass()));

        sceneSwitcher.loadScene("Rfid");


        stage.show();
}


    public static void main(String[] args) {
        launch(args);

    }
}

